import random


class Des:
    def __init__(self, nb_faces):
        self.nb_faces = nb_faces
        self.score = 0

    def __str__(self):
        return f"Des avec {self.nb_faces} faces, dernier score : {self.score}"

    def lancer(self):
        self.score = random.randint(1, self.nb_faces)

    def get_score(self):
        return self.score