import random


class Jeu_de_des:
    def __init__(self, nb_des):
        self.nb_des = nb_des
        self.resultats = []

    def __str__(self):
        return f"Jeu de dés avec {self.nb_des} dés, résultats : {self.resultats}"

    def lancer(self):
        self.resultats = [random.randint(1, 6) for _ in range(self.nb_des)]
        print(f"Les dés ont été lancés. Résultats : {self.resultats}")

    def save(self):
        with open("resultats_lancers.txt", "w") as file:
            for i, resultat in enumerate(self.resultats, 1):
                file.write(f"Lancement du dé {i} : {resultat}\n")

            file.write(f"\nVotre score est de {self.get_score()}.")

        print("Les résultats des lancers ont été enregistrés dans 'resultats_lancers.txt'.")

    def get_score(self):
        return sum(self.resultats)