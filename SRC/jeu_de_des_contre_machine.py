import datetime
import os
import random


class Jeu_de_des_contre_machine:
    def __init__(self, nb_des, nom_joueur):
        self.nb_des = nb_des
        self.score_joueur = 0
        self.nom_joueur = nom_joueur
        self.score_machine = 0

    def __str__(self):
        return f"Jeu de dés contre la machine avec {self.nb_des} dés, joueur : {self.nom_joueur}, score joueur : {self.score_joueur}, score machine : {self.score_machine}"

    def lancer(self):
        resultats_joueur = [random.randint(1, 6) for _ in range(self.nb_des)]
        resultats_machine = [random.randint(1, 6) for _ in range(self.nb_des)]
        self.score_joueur = sum(resultats_joueur)
        self.score_machine = sum(resultats_machine)
        return resultats_joueur, resultats_machine

    def get_score(self):
        return self.score_joueur, self.score_machine

    def jouer(self):
        resultats_joueur, resultats_machine = self.lancer()
        print(f"Vos résultats : {resultats_joueur}")
        print(f"Résultats de la machine : {resultats_machine}")
        print(f"Votre score : {self.score_joueur}, Score de la machine : {self.score_machine}")

    def save(self, filename):
        now = datetime.datetime.now()
        filename = f"{filename}_{now.strftime('%Y-%m-%d_%H-%M-%S')}.txt"
        with open(filename, "w") as file:
            file.write(f"Nom du joueur;Score du joueur;Score de la machine\n")
            file.write(f"{self.nom_joueur};{self.score_joueur};{self.score_machine}\n")
        print(f"Les scores ont été enregistrés dans '{filename}'.")

    def load(self, filename):
        if os.path.exists(filename):
            with open(filename, "r") as file:
                lines = file.readlines()
                header = lines[0].split(";")
                data = lines[1].split(";")
                self.nom_joueur = data[0]
                self.score_joueur = int(data[1])
                self.score_machine = int(data[2])
        else:
            print(f"Le fichier '{filename}' n'existe pas. Un nouveau fichier sera créé lors de la sauvegarde.")


    def afficher(self):
        print(f"Nom du joueur : {self.nom_joueur}")
        print(f"Score du joueur : {self.score_joueur}")
        print(f"Score de la machine : {self.score_machine}")