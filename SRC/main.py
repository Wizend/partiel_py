import os
from des import Des
from jeu_de_des import Jeu_de_des
from jeu_de_des_contre_machine import Jeu_de_des_contre_machine

import tkinter as tk
from tkinter import messagebox, simpledialog, filedialog


class Main(tk.Tk):
    def __init__(self):
        super().__init__()

        # Configuration de la fenêtre principale
        self.title("Jeu de Dés")
        self.geometry("400x400")

        # Libellé de bienvenue
        self.label = tk.Label(self, text="Bienvenue dans le Jeu de Dés", font=("Helvetica", 16))
        self.label.pack(pady=10)

        # Zone de texte pour les résultats
        self.result_text = tk.Text(self, height=10, width=40)
        self.result_text.pack(pady=10)

        # Cadre pour les boutons
        self.button_frame = tk.Frame(self)
        self.button_frame.pack()

        # Liste pour stocker les boutons d'exercice
        self.exercise_buttons = []
        for i in range(1, 5):
            btn = tk.Button(self.button_frame, text=f"Exercice {i}", command=lambda i=i: self.run_exercise(i))
            btn.grid(row=0, column=i-1, padx=5, pady=5)
            self.exercise_buttons.append(btn)

    def run_exercise(self, exercise_num):
        # Efface le contenu précédent de la zone de texte des résultats
        self.result_text.delete(1.0, tk.END)

        try:
            # Exécute l'exercice en fonction du numéro fourni en argument
            if exercise_num == 1:
                self.exercice1()
            elif exercise_num == 2:
                self.exercice2()
            elif exercise_num == 3:
                self.exercice3()
            elif exercise_num == 4:
                self.exercice4()
        except Exception as e:
            # Gère les erreurs et affiche un message d'erreur
            messagebox.showerror("Erreur", f"Une erreur s'est produite : {str(e)}")

    def exercice1(self):
        try:
            # Demande à l'utilisateur le nombre de faces du dé
            nb_faces = simpledialog.askinteger("Exercice 1", "Entrez le nombre de faces du dé :")
            if nb_faces is not None:
                # Crée un objet de la classe Des avec le nombre de faces spécifié
                de = Des(nb_faces)
                # Lance le dé et affiche le résultat dans la zone de texte des résultats
                de.lancer()
                result = f"Exercice 1 : Le dé a été lancé. Résultat : {de.get_score()}\n"
                self.result_text.insert(tk.END, result)
        except Exception as e:
            # Gère les erreurs et affiche un message d'erreur
            messagebox.showerror("Erreur", f"Une erreur s'est produite dans l'exercice 1 : {str(e)}")

    def exercice2(self):
        try:
            # Demande à l'utilisateur le nombre de dés à lancer
            nb_des = simpledialog.askinteger("Exercice 2", "Entrez le nombre de dés à lancer :")
            if nb_des is not None:
                # Crée un objet de la classe Jeu_de_des avec le nombre de dés spécifié
                jeu = Jeu_de_des(nb_des)
                # Lance les dés du jeu, affiche les résultats et le score dans la zone de texte des résultats
                jeu.lancer()
                result = f"Exercice 2 : Les dés ont été lancés. Résultats : {jeu.resultats}\nScore du jeu : {jeu.get_score()}\n"
                self.result_text.insert(tk.END, result)
        except Exception as e:
            # Gère les erreurs et affiche un message d'erreur
            messagebox.showerror("Erreur", f"Une erreur s'est produite dans l'exercice 2 : {str(e)}")

    def exercice3(self):
        try:
            # Demande à l'utilisateur le nombre de lancers et le nombre de dés par lancer
            nb_lances = simpledialog.askinteger("Exercice 3", "Entrez le nombre de lancers :")
            nb_des_par_lancers = simpledialog.askinteger("Exercice 3", "Entrez le nombre de dés par lancers :")
            if nb_lances is not None:
                # Crée un objet de la classe Jeu_de_des avec le nombre de dés par lancer spécifié
                jeu = Jeu_de_des(nb_des_par_lancers)
                results_text = ""

                for _ in range(nb_lances):
                    # Lance les dés du jeu et ajoute les résultats au texte des résultats
                    jeu.lancer()
                    results_text += f"Les dés ont été lancés. Résultats : {jeu.resultats}\n"

                # Affiche les résultats dans la zone de texte des résultats
                result = f"Exercice 3 : Les résultats des lancers ont été générés.\n{results_text}"
                self.result_text.insert(tk.END, result)

                # Demande à l'utilisateur s'il souhaite enregistrer les résultats
                save_results = messagebox.askyesno("Enregistrer les Résultats", "Voulez-vous enregistrer les résultats?")

                if save_results:
                    # Obtient le répertoire du script en cours d'exécution comme répertoire initial
                    initial_dir = os.getcwd()
                    # Ouvre une boîte de dialogue pour obtenir le nom et l'emplacement du fichier
                    filename = filedialog.asksaveasfilename(
                        defaultextension=".txt", filetypes=[("Text files", "*.txt")],
                        initialdir=initial_dir
                    )
                    if filename:
                        # Enregistre les résultats dans le fichier spécifié
                        with open(filename, "w") as file:
                            file.write(results_text)
                        # Affiche un message de confirmation
                        messagebox.showinfo("Sauvegarde", f"Les résultats ont été enregistrés dans '{filename}'.")
                
        except Exception as e:
            # Gère les erreurs et affiche un message d'erreur
            messagebox.showerror("Erreur", f"Une erreur s'est produite dans l'exercice 3 : {str(e)}")

    def exercice4(self):
        try:
            # Demande à l'utilisateur son nom et le nombre de dés pour l'exercice 4
            nom_joueur = simpledialog.askstring("Exercice 4", "Entrez votre nom :")
            nb_des_str = simpledialog.askstring("Exercice 4", "Entrez le nombre de des :")
            if nom_joueur is not None and nb_des_str is not None:
                # Convertit la chaîne du nombre de dés en entier
                nb_des = int(nb_des_str)
                # Crée un objet de la classe Jeu_de_des_contre_machine avec le nombre de dés et le nom du joueur spécifiés
                jeu = Jeu_de_des_contre_machine(nb_des, nom_joueur)
                # Lance le jeu contre la machine
                jeu.jouer()

                # Affiche un message indiquant que les scores ont été générés
                result = f"Exercice 4 : Les scores ont été générés.\n"
                self.result_text.insert(tk.END, result)

                # Demande à l'utilisateur s'il souhaite enregistrer les scores
                save_scores = messagebox.askyesno("Enregistrer les Scores", "Voulez-vous enregistrer les scores?")
                
                if save_scores:
                    # Obtient le répertoire du script en cours d'exécution comme répertoire initial
                    initial_dir = os.getcwd()
                    # Ouvre une boîte de dialogue pour obtenir le nom et l'emplacement du fichier
                    filename = filedialog.asksaveasfilename(
                        filetypes=[("Text files", "*.txt")],
                        initialdir=initial_dir
                    )
                    if filename:
                        # Enregistre les scores dans le fichier spécifié
                        jeu.save(filename)
                        # Charge les scores depuis le fichier
                        jeu.load(filename)
                        # Affiche les scores dans la zone de texte des résultats
                        jeu.afficher()
                        # Affiche un message de confirmation
                        messagebox.showinfo("Sauvegarde", f"Les scores ont été enregistrés dans '{filename}'.")
                
        except Exception as e:
            # Gère les erreurs et affiche un message d'erreur
            messagebox.showerror("Erreur", f"Une erreur s'est produite dans l'exercice 4 : {str(e)}")


if __name__ == "__main__":
    # Crée une instance de la classe Main et lance l'application
    app = Main()
    app.mainloop()

